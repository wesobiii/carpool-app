;(function(){
    "use strict";

    angular.module('app',
        [
            'app.controllers',
            'app.filters',
            'app.services',
            'app.directives',
            'app.routes',
            'app.config',
            'app.selfAuthenticate'
        ]);

    angular.module('app.routes', ['ui.router', 'ngStorage', 'ngMaterial']);
    angular.module('app.controllers', ['ui.router', 'ngMaterial', 'ngStorage', 'restangular']);
    angular.module('app.filters', []);
    angular.module('app.services', ['ui.router', 'ngStorage', 'restangular']);
    angular.module('app.directives', []);
    angular.module('app.config', []);
    angular.module('app.selfAuthenticate', []);
})();