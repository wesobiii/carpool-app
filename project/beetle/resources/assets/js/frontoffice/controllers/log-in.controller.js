;(function () {'use strict';

    angular.module('app.controllers', [])
        .controller('ExampleController', ExampleController);

    ExampleController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$window',
        // Custom
        'AuthUserResourceFactory',
        'DialogFactory',
        'StorageFactory',
        'ToastFactory',
        'UserModelFactory'
    ];


    function ExampleController(

        $log,
        $scope,
        $window,
        // Custom
        AuthUserResourceFactory,
        DialogFactory,
        StorageFactory,
        ToastFactory,
        UserModelFactory

        ) {
        $scope.master = {};

        $scope.update = function(user) {
            $scope.master = angular.copy(user);
        };

        var vm = this;

        vm.processForm = processForm;

        vm.user = new UserModelFactory();

        function loginError(reason) {
            $log.error('loginError:', reason);
            DialogFactory
                .showItems('Could not log you in!', reason.data);
        }

        function loginSuccess(response) {
            $log.log('loginSuccess:', response);
            ToastFactory
                .show('User logged in!')
                .finally(function() {
                    vm.user.id = response.id;
                    StorageFactory.Local.setObject('user', vm.user);
                    $window.location.href = '#/settings';
                });
        }

        function processForm(event) {
            event.preventDefault();
            if ($scope.login_form.$valid) {
                $log.log('user: ', vm.user);

                var authUserResource = new AuthUserResourceFactory();
                authUserResource.user = vm.user;
                authUserResource.$login()
                    .then(loginSuccess)
                    .catch(loginError);
            } else {
                ToastFactory
                    .show('Please fill in the required fields to continue!');
            }
        }
    }

})();