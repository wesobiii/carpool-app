@extends('headermenu')

@section('content')

<div class="content-container">
    <div class="row">
        <p class="steps_title">Step 3/3</p>
    </div>

    {!! Form::open() !!}
    <div class="form-group txt-input">
      <div class="col-lg-1 col-xs-1 ">
        {!! Form::label('seats', 'Seats') !!}
      </div>

      <div class="col-lg-9 col-xs-10 col-xs-offset-1">
            {!! Form::text('seats', null, ['class' => 'form-control', 'placeholder' => '3']) !!}
      </div>
    </div>

     <div class="form-group txt-input">
          <div class="col-lg-1 col-xs-1 ">
            {!! Form::label('bagage', 'Bagage') !!}
          </div>

          <div class="col-lg-9 col-xs-10 col-xs-offset-1">
                {!! Form::text('bagage', null, ['class' => 'form-control', 'placeholder' => 'medium']) !!}
          </div>
     </div>

    <div class="form-group txt-input">
          <div class="col-lg-1 col-xs-1 ">
            {!! Form::label('talking', 'Talking') !!}
          </div>

          <div class="col-lg-9 col-xs-10 col-xs-offset-1">
        {!! Form::checkbox('Talking') !!}
          </div>
    </div>

    <div class="form-group txt-input">
          <div class="col-lg-1 col-xs-1 ">
            {!! Form::label('smoking', 'Smoking') !!}
          </div>

          <div class="col-lg-9 col-xs-10 col-xs-offset-1">
        {!! Form::checkbox('Smoking') !!}
          </div>
    </div>

       <div class="form-group txt-input">
              <div class="col-lg-1 col-xs-1 ">
                {!! Form::label('music', 'Music') !!}
              </div>

              <div class="col-lg-9 col-xs-10 col-xs-offset-1">
            {!! Form::checkbox('Music') !!}
              </div>
        </div>



    <div class="row">
        <div class="col-xs-3 col-xs-offset-2">
            {!!Form::button('Previous', ['class' => 'btn button-next']) !!}
        </div>

        <div class="col-xs-offset-3 col-xs-3">
            {!!Form::button('Next', ['class' => 'btn button-next']) !!}
        </div>
    </div>
    {!! Form::close() !!}
 </div>


@stop
