@extends('layouts.frontoffice')


@section('content')

<div id="dashboard-options" class="content-container">
    <div class="row">


        <a href="">
            <div class="dashboard-button col-xs-offset-1 col-xs-10">
                <i class="fa fa-compass fa-2x"></i>
                <p>Share a ride</p>
            </div>
        </a>
    </div>

    <div class="row">
        <a href="#">
            <div class="dashboard-button col-xs-offset-1 col-xs-10">
                <i class="fa fa-search fa-2x"></i>
                <p>search a ride</p>
            </div>
        </a>
    </div>
</div>
@stop