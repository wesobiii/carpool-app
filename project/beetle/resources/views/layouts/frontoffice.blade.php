<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
	<meta charset="UTF-8">
	<title>frontoffice.blade.php</title>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
@section('head-styles')
	{!! Html::style('//fonts.googleapis.com/css?family=Lato:100') !!}
	{!! Html::style('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') !!}
	{!! Html::style('css/frontoffice.css') !!}
	{!! Html::style('css/angular.css') !!}
	{!! Html::style('css/bootstrap.css') !!}
	{!! Html::style('css/all.css') !!}
@show
@section('head-scripts')
    {!! Html::script('js/angular.js') !!}
    {!! Html::script('js/frontoffice.js') !!}
    {{--{!! Html::script('js/app.js') !!}--}}
    {!! Html::script('js/vendor.js') !!}
@show
</head>
<body>
<input type="hidden" id="csrf-token" value="{{ csrf_token() }}" />
    @include('partials.nav')
    <div class="container">
		<div class="row">
		    @yield('content')
		</div>
	</div>
</body>
</html>