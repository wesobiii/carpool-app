;(function(){
    "use strict";

    angular.module('app',
        [
            'app.controllers',
            'app.filters',
            'app.services',
            'app.directives',
            'app.routes',
            'app.config',
            'app.selfAuthenticate'
        ]);

    angular.module('app.routes', ['ui.router', 'ngStorage', 'ngMaterial']);
    angular.module('app.controllers', ['ui.router', 'ngMaterial', 'ngStorage', 'restangular']);
    angular.module('app.filters', []);
    angular.module('app.services', ['ui.router', 'ngStorage', 'restangular']);
    angular.module('app.directives', []);
    angular.module('app.config', []);
    angular.module('app.selfAuthenticate', []);
})();
;(function(){
    "use strict";

    angular.module('app.routes').config( ["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider ) {

        var getView = function( viewName ){
            return '/views/' + viewName + '.html';
        };

        $urlRouterProvider.otherwise('/');

        $stateProvider

            .state('landing', {
                url: '/',
                views: {
                    main: {
                        templateUrl: getView('dashboard/index')
                    }
                }
            })
            .state('rides', {
                url: '/rides',
                views: {
                    main: {
                        templateUrl: getView('rides/index')
                    }
                }
            })
            .state('trophies', {
                url: '/trophies',
                views: {
                    main: {
                        templateUrl: getView('trophies/index')
                    }
                }
            })
            .state('settings', {
                url: '/settings',
                views: {
                    main: {
                        templateUrl: getView('settings/index')
                    }
                }
            })
            .state('about', {
                url: '/settings/about',
                views: {
                    main: {
                        templateUrl: getView('settings/about')
                    }
                }
            })
            .state('profile', {
                url: '/settings/profile',
                views: {
                    main: {
                        templateUrl: getView('settings/profile')
                    }
                }
            });


    }] );
})();
;(function () {'use strict';

    angular.module('app.controllers', [])
        .controller('ExampleController', ExampleController);

    ExampleController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$window',
        // Custom
        'AuthUserResourceFactory',
        'DialogFactory',
        'StorageFactory',
        'ToastFactory',
        'UserModelFactory'
    ];


    function ExampleController(

        $log,
        $scope,
        $window,
        // Custom
        AuthUserResourceFactory,
        DialogFactory,
        StorageFactory,
        ToastFactory,
        UserModelFactory

        ) {
        $scope.master = {};

        $scope.update = function(user) {
            $scope.master = angular.copy(user);
        };

        var vm = this;

        vm.processForm = processForm;

        vm.user = new UserModelFactory();

        function loginError(reason) {
            $log.error('loginError:', reason);
            DialogFactory
                .showItems('Could not log you in!', reason.data);
        }

        function loginSuccess(response) {
            $log.log('loginSuccess:', response);
            ToastFactory
                .show('User logged in!')
                .finally(function() {
                    vm.user.id = response.id;
                    StorageFactory.Local.setObject('user', vm.user);
                    $window.location.href = '#/settings';
                });
        }

        function processForm(event) {
            event.preventDefault();
            if ($scope.login_form.$valid) {
                $log.log('user: ', vm.user);

                var authUserResource = new AuthUserResourceFactory();
                authUserResource.user = vm.user;
                authUserResource.$login()
                    .then(loginSuccess)
                    .catch(loginError);
            } else {
                ToastFactory
                    .show('Please fill in the required fields to continue!');
            }
        }
    }

})();