<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*PAGES SET UP*/

    /*pages of the headermenu*/

Route::get('rides', 'PagesController@rides');

//Route::get('/', 'PagesController@dashboard');

    /*pages for sharing a ride*/

Route::get('share_one','PagesController@share_one');

Route::get('share_two', 'PagesController@share_two');

Route::get('share_three', 'PagesController@share_three');

Route::get('share_four','PagesController@share_four');



// Settings Routes
// ===================
Route::group(
    ['prefix' => 'settings'],
    function() {
        Route::get('/', 'SettingsController@index');
        Route::get('about', 'SettingsController@about');
        Route::get('account', 'SettingsController@account');
        Route::get('admin', 'SettingsController@admin');
        Route::get('disclaimer', 'SettingsController@disclaimer');
        Route::get('profile', 'SettingsController@profile');
    }
);


// Auth Routes
// ===================
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/', 'MainController@index');