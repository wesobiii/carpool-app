<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Ride;
use Illuminate\Http\Request;

class PagesController extends Controller {

	//HEADER/MENU PAGES
    public function rides()
    {

        $test = 'wesley vanbrabant';

        return view('pages.rides')->with('test', $test);

    }

    public function dashboard()
    {
        $rides = Ride::all();

        return view('pages.dashboard', compact('rides'));
    }

    //Pages concerning the sharing of a ride
    public function share_one()
    {
        return view('pages.share.one');
    }

    public function share_two()
    {
        return view('pages.share.two');
    }

    public function share_three()
    {
        return view('pages.share.three');
    }

    public function share_four()
    {
        $success = true;

        return view('pages.share.four', compact('success'));
    }


    //pages concerning the settings

    public function about()
    {
        return view('pages.settings.about');
    }

    public function admin()
    {
        $user = 'admin';

        return view('pages.settings.admin', compact('user'));
    }

    public function account()
    {
        return view('pages.settings.account');
    }

    public function profile()
    {
        return view('pages.settings.profile');
    }

    public function disclaimer()
    {
        return view('pages.settings.disclaimer');
    }


}
