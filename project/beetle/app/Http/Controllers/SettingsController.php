<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class SettingsController extends Controller {

    /**
     * Create a new articles controller instance
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages.settings.index');
    }

    public function about()
    {
        return view('pages.settings.about');
    }

    public function admin()
    {
        $user = 'admin';

        return view('pages.settings.admin', compact('user'));
    }

    public function account()
    {
        return view('pages.settings.account');
    }

    public function profile()
    {
        return view('pages.settings.profile');
    }

    public function disclaimer()
    {
        return view('pages.settings.disclaimer');
    }

}
