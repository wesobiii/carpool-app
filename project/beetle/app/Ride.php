<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ride extends Model {

    //attributes you are ok with being mass-assigned.
    //NEVER ID, AND SIMILAR.
	protected $fillable = [
        'creator',
        'seats',
        'start',
        'destination'
    ];

}
