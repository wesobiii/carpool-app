var elixir = require ('laravel-elixir');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir.config.sourcemaps = false;
elixir.config.jsBaseDir     = './';


var vendor = {
        angular         : elixir.config.bowerDir + '/angular/angular.js',
        angularModule   : {
            animate     : elixir.config.bowerDir + '/angular-animate/angular-animate.js',
            aria        : elixir.config.bowerDir + '/angular-aria/angular-aria.js',
            cookies     : elixir.config.bowerDir + '/angular-cookies/angular-cookies.js',
            material    : elixir.config.bowerDir + '/angular-material/',
            messages    : elixir.config.bowerDir + '/angular-messages/angular-messages.js',
            resource    : elixir.config.bowerDir + '/angular-resource/angular-resource.js',
            route       : elixir.config.bowerDir + '/angular-route/angular-route.js',
            sanitize    : elixir.config.bowerDir + '/angular-sanitize/angular-sanitize.js',
            touch       : elixir.config.bowerDir + '/angular-touch/angular-touch.js',
            mocks       : elixir.config.bowerDir + '/angular-mocks/angular-mocks.js'
        }
};


elixir(function(mix) {
   mix
       .copy(
        vendor.angularModule.material + 'angular-material.css',
        elixir.config.cssOutput + '/angular.css'
        )

        .copy(
            elixir.config.assetsDir + 'sass/fontawesome/fonts',
            elixir.config.publicDir + '/fonts'
        )


        .scripts([
            vendor.angular,
            vendor.angularModule.animate,
            vendor.angularModule.aria,
            vendor.angularModule.cookies,
            vendor.angularModule.material + 'angular-material.js',
            vendor.angularModule.messages,
            vendor.angularModule.resource,
            vendor.angularModule.route,
            vendor.angularModule.sanitize,
            vendor.angularModule.touch,
            vendor.angularModule.mocks
        ],
            elixir.config.jsOutput + '/angular.js',
            elixir.config.jsBaseDir
        )

       .scriptsIn(
           elixir.config.assetsDir + 'js/frontoffice',
           elixir.config.jsOutput + '/frontoffice.js'
       );


    //mix.scripts(['frontoffice/'], "public/js");

    mix.sass(["frontoffice.scss"], "public/css");

    mix.styles(['angular.css', 'frontoffice.css'], null, elixir.config.cssOutput);

    mix.version(elixir.config.cssOutput + '/all.css');



});
