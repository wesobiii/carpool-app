Carpool app
===================

----------

## Inhoudstafel

[TOC]

## Deadlines

Datum    | To do
-------- | ---
20 februari   | Design probeersels
20 februari   | Hou je tijd bij met https://www.toggl.com/
27 februari   | Ideaboard
27 februari   | Personas
27 februari   | Sitemap
27 februari   | Wireframes
27 februari   | Moodboard
27 februari   | 3 Style Tiles
06 maart   | Screen Designs
25 mei   | Indienen productiedossier
?? juni  | Presentatie van het werkstuk
?? juni  | Indienen academische poster

## Opdracht

https://stackedit.io/viewer#!provider=gist&gistId=5c7c274c0ff479c98d06&filename=NMDAD%20II%20-%20Briefing.md

> **Probleemstelling**
>
> Hoe kunnen Gentse studenten aangespoord worden om te carpoolen naar een campus van een hogeschool of universiteit?

Ontwerp en implementeer in groep (2 tot 3 leden) een mobile first, responsive web app om te carpoolen naar een campus van een hogeschool of universiteit in Gent. De frontoffice van de web app moet de gebruikers aanzetten tot frequenter te carpoolen door gamificatie toe te passen (bijv. scores, leaderboard, badges, quests, enz.). De backoffice moet toelaten dat de beheerder van de web app gebruikers kan beheren en nuttige statistieken kan bekijken over de bezoekers/gebruikers.

Verwerk open data van de stad Gent in de web app.

Bekijk ter inspiratie bestaande carpool websites zoals (maar niet beperkt tot) www.carpool.be en http://www.blablacar.nl/.

## To do

### Productiedossier

- Voorblad
 - Checklist met alle gevraagde onderdelen van deliverables en features
 - Briefing
 - Planning
 - Timesheet per teamlid (tijdsregistratie van de uren die aan dit opleidingsonderdeel besteed hebt)
 - Ideaboard
 - Concept
 - Personas
 - Sitemap
 - Wireframes
 - Visueel ontwerp
   - Moodboard
   - Style Tiles (minstens 3 en duid aan welke gekozen werd)
   - Screen Designs
 - Link naar het Bitbucket-repository

## Ideeën

### Naam

 - Free Wheely
 - RidesWithBenefits
 - Carlord
 - BattleOfShares

### Databanken

http://data.gent.be/datasets/buurtparking
http://data.gent.be/datasets/parkeren-bewonerskaartzones
http://data.gent.be/datasets/parkeercapaciteit
http://data.gent.be/datasets/parkinglocaties-gent

### Gamificatie

 - Leaderboard
 - Quests
 - Experience, level up.

### Specificaties

#### Verplicht

##### Beheerder

 - Aanmelden met gebruikersnaam en wachtwoord
 - Afmelden
 - Gebruiker toevoegen
 - Gebruiker verwijderen (soft delete)
 - Gebruiker schorsen, zodat de gebruiker (tijdelijk) niet meer kan aanmelden
 - Gebruikersstatistieken raadplegen

##### Gebruiker

 - Profiel aanmaken: gebruikersnaam, wachtwoord, profielfoto toevoegen, naam, contactgegevens, adresgegevens
 - Profielgegevens wijzigen
 - Aanmelden met e-mailadres en wachtwoord
 - Afmelden
 - Andere gebruikers zoeken volgens criteria 
 - Friend Request sturen naar andere gebruikers
 - Friend Request accepteren

#### Extras

 - Gamificatie
 - Datasets
 - Tinder principe?
 - Wachtwoord alternatief (android cirkels)